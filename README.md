# 空中音乐坊

#### 介绍

澎雅小学“空中音乐坊”以Gitee的项目仓库作为入口，提供在线课程，并展示学习成果，是一种“互联网+音乐”的平台。它是学校音乐学习的新路径与对学校音乐教学的补足，为孩子学习音乐、展示音乐、评价音乐等开辟了新的通道。

#### 资源链接

1.360艺术测评：

http://www.360yishu.com

2.国家中小学网络云平台：

https://ykt.eduyun.cn/ykt/sjykt/index.html?from=timeline&isappinstalled=0

3.澎雅小学哔哩哔哩网站：

https://live.bilibili.com/h5/21928292

4.澎雅小学微信公众号链接：

https://mp.weixin.qq.com/s/NxhnLXWcqe28JyyD1wgpDw

5.音乐电影推荐：

https://v.qq.com/x/cover/u6f293t3bt9sncj.html
https://www.iqiyi.com/v_19rr7cblhg.html
https://v.qq.com/x/cover/vjrvzv3s517g6m8.html?pcsharecode=&sf=uri
https://v.qq.com/x/search/?q=%E5%AE%AB%E5%A5%87%E5%B3%BB%E5%8A%A8%E7%94%BB%E7%89%87&stag=0&smartbox_ab=
https://url.cn/5XazON4?sf=uri 

6.戏曲推荐

https://haokan.baidu.com/v?vid=13307337997796172222&pd=bjh&fr=bjhauthor&type=video
http://www.iqiyi.com/w_19rs0zmit9.html
https://haokan.baidu.com/v?vid=5426130616037271446&pd=bjh&fr=bjhauthor&type=video
https://haokan.baidu.com/v?vid=7326639090327016690&pd=bjh&fr=bjhauthor&type=video
https://haokan.baidu.com/v?vid=8839384047207552320&pd=bjh&fr=bjhauthor&type=video



